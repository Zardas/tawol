
game.prototype.genItem= function(){

	
	
	for (I=0; I<this.levelGame*10; I++) {
		var xAlea = Math.floor(Math.random()* (16 - 0) + 0)+1;
		var yAlea = Math.floor(Math.random()* (this.levelGame*10 - 0) + 0)+1;
		
		var itemChoix = Math.floor(Math.random()* (100 - 0) + 0);
		if(itemChoix <= this.probaItem*this.levelGame)
		{
			myItem =new item("Images/item2.png",-20,xAlea*51,-yAlea*51,4, 50, 50);
		}
		else
		{
			myItem =new item("Images/item1.png",10,xAlea*51,-yAlea*51,4, 50, 50);
		}
		MyItemTab.push(myItem);

	}
}

function game() {

	var canvas = document.getElementById("canvas");
	
	MyItemTab = new Array();
	
	canvas.width=1000;
	canvas.height=600;

	this.niveauMenu= 2;
	
	this.posXEventMouse = 0;
	this.posYEventMouse = 0;

	this.levelGame = 5;
	this.probaItem = 5 ;
	
	this.genItem();


}




game.prototype.menu = function(){

	context.font = "70px italic";
	context.fillText("The American Way Of Life",100,100);
	
	context.font = "70px italic";
	context.fillText("History",350,200);
	
	context.font = "70px italic";
	context.fillText("Survival",350,300);
	
	context.font = "70px italic";
	context.fillText("Settings",350,400);
	
	context.font = "70px italic";
	context.fillText("Credit",350,500);
}


game.prototype.setNiveauMenu = function(lvl){

	this.niveauMenu=lvl;
}

game.prototype.run = function(){


	if(this.niveauMenu==1)
	{
		context.clearRect ( 0 , 0 , canvas.width, canvas.height );
		this.menu();
	}
	else if(this.niveauMenu==2)
	{
	
	
	
		myHud.drawBackground();
		myPersonnage.draw();
		
		
		for (i=0; i<myGame.getLevelGame()*10; i++) {
			MyItemTab[i].run() 
		}
		
		myHud.update();
		myHud.drawHud();

	}
	else if(this.niveauMenu==3)
	{
		this.survival();
	}
	else if(this.niveauMenu==4)
	{
		this.option();
	}
	else if(this.niveauMenu==5)
	{
		this.credit();
	}

}

game.prototype.getLevelGame = function(){
	return this.levelGame;
}

game.prototype.setLevelGame = function(){
	this.levelGame = this.levelGame +1;
}
