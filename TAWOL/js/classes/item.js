function item(image,point,axeX,axeY, vitesseY,height,width) {

	this.type= 1;
	this.imageItem = new Image();
	this.imageItem.src= image;
	this.point = point;
	this.axeX = axeX;
	this.axeY = axeY;	 
	this.vitesseY = vitesseY;
	this.height = height;
	this.width = width;
	this.active = 1;
}


item.prototype.draw = function(){
	
	if(this.active)
	{
		if(this.axeY<=canvas.height){

			context.drawImage(this.imageItem,this.axeX,this.axeY);
			this.addAxeY();
			
			
		}
	}

}

item.prototype.addAxeY = function(){
	
	this.axeY =this.axeY + this.vitesseY;	

	
}

item.prototype.collide = function(){

	if((this.axeY+this.height >=myPersonnage.getPosY() && this.axeY+this.height <=myPersonnage.getPosY()+myPersonnage.getHeight() && this.axeX >= myPersonnage.getPosX() && this.axeX <= myPersonnage.getPosX()+myPersonnage.getWidth() 
	|| this.axeY+this.height >=myPersonnage.getPosY() && this.axeY+this.height <=myPersonnage.getPosY()+myPersonnage.getHeight() && this.axeX+this.width >= myPersonnage.getPosX() && this.axeX+this.width <= myPersonnage.getPosX()+myPersonnage.getWidth()) 
	&& this.active == 1)
	{
		
		this.active = 0;
		myHud.addPoint(this.point);
		this.imageItem.src="";
		
		
	}

}

item.prototype.run = function(){
	this.collide();
	this.draw();
}



