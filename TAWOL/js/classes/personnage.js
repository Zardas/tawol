function personnage(image, nom) {
	this.image = new Image();
	this.image.src = image;
	this.nom = nom;
	this.positionX = 400;
	this.positionY = 450;
	this.height = 150;
	this.width = 200;
}

personnage.prototype.draw = function() {
	context.drawImage(this.image, this.positionX,this.positionY);

}

personnage.prototype.setPosX = function(posX) {
	this.positionX = posX;
}

personnage.prototype.getWidth = function() {
	return this.width;
}

personnage.prototype.getPosX = function() {
	return this.positionX;
}

personnage.prototype.getPosY = function() {
	return this.positionY;
}

personnage.prototype.getHeight = function() {
	return this.height;
}