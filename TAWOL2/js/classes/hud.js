function hud(lvlmax) {

	this.imageFond = new Image();
	this.imageFond.src= "Images/Fond.png";
	
	this.imageScoreBarre = new Image();
	this.imageScoreBarre.src= "Images/barrescore.png";
	
	this.imageAffichageNiveau = new Image();
	this.imageAffichageNiveau.src= "Images/affichageNiveau.png";
	
	this.niveauMax = lvlmax;
	this.niveau = 0;
	this.niveauRatio = 0;



}


hud.prototype.drawBackground = function(){

	context.drawImage(this.imageFond,0,0);

}

hud.prototype.addPoint = function(point){

	this.niveau = this.niveau + point;

}

hud.prototype.drawHud = function(){

	
	context.fillStyle = "rgb(200,0,0)";
	context.fillRect (915, 385, 40, -this.niveauRatio);
	
	context.drawImage(this.imageScoreBarre,900,50);
	
	context.drawImage(this.imageAffichageNiveau,-75,-75);
	context.font = "70px italic";
	context.fillText(myGame.getLevelGame(),0,50);



}	
hud.prototype.update = function(){

	this.niveauRatio =  this.niveau*325/this.niveauMax;
	if ( this.niveauRatio >= 325)
	{
	
		MyItemTab = [];

		context.font = "70px italic";
		context.fillText("You win !",300,350);
		
		myGame.setLevelGame(1);
		this.niveauRatio=0;
		this.niveau=0;
		this.setLvlMax();
		myGame.genItem();
	}
	else if(this.niveauRatio < -50*myGame.getLevelGame())
	{
		context.font = "70px italic";
		context.fillText("You lose !",300,350);
		MyItemTab = [];
		this.niveauRatio=0;
		this.niveau=0;
		myGame.genItem();
	}
	else if(myGame.getItemLost() > myGame.getLevelGame()*10-1)
	{
		context.font = "70px italic";
		context.fillText("You lose !",300,350);
		MyItemTab = [];
		this.niveauRatio=0;
		this.niveau=0;
		myGame.genItem();
	}
	

}

hud.prototype.resetBarre = function(){
	this.niveauRatio=0;
}

hud.prototype.setLvlMax = function(){
	this.niveauMax = myGame.getLevelGame()*10*0.8;
}
